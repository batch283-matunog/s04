-- Command to access MariaDB in cmd.
mysql -u root

-- Lists down the databases inside the RDBMS
-- No need to show database if you are certain in the name of the database you are going to use.
SHOW DATABASES;

-- Select a database to manipulate.
USE music_db;

-- Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that has a length of less than 3 mins and 50 sec
SELECT * FROM songs WHERE length < "00:03:50";

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length FROM albums 
  JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists 
  JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- Sort the albums in Z-A order.(Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums
  JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;  